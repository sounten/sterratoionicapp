import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'calendario2', loadChildren: './pages/calendario2/calendario2.module#Calendario2PageModule' },
  { path: 'formulario/:data', loadChildren: './pages/formulario/formulario.module#FormularioPageModule' },  { path: 'newsletter', loadChildren: './pages/newsletter/newsletter.module#NewsletterPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
