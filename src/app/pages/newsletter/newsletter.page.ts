import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { FormularioPage } from '../formulario/formulario.page';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.page.html',
  styleUrls: ['./newsletter.page.scss'],
})
export class NewsletterPage implements OnInit {

  emailPattern = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);
  isEmail = false;
  email;
  shareEmail = false;
  // tslint:disable-next-line:max-line-length
  info = 'Suscribiéndote a nuestra newsletter estarás siempre al corriente de las últimas noticias de nuestro establecimiento, incluyendo ofertas, promociones, descuentos, actividades y más.';
  // tslint:disable-next-line:max-line-length
  privacyPolicyText = 'He leído y acepto las condiciones establecidas en la <a href="http://privacy-policy-sterrato.s3-website.eu-west-3.amazonaws.com/">política de privacidad</a>.';

  constructor(  private toastController: ToastController,
                private loadingController: LoadingController,
                private alertController: AlertController,
                private navController: NavController,
                private dataService: DataService) { }

  ngOnInit() {
  }

  isValidEmail() {
    this.isEmail = this.emailPattern.test(this.email);
    console.log('isemail value: ', this.isEmail);
  }

  subscribe() {
    if (!this.isEmail) {
      this.presentToast('Formato de e-mail no válido');
    } else {
      this.postNewsletter(this.email);
    }

  }

  unSubscribe() {
    if (!this.isEmail) {
      this.presentToast('Formato de e-mail no válido');
    } else {
      this.deleteNewsletter(this.email);
    }

  }

  async postNewsletter(email: any) {

    const loader = await this.loadingController.create({
      message: 'Procesando suscripción...',
    });
    await loader.present();

    this.dataService.postNewsletter(email).toPromise().then((data) => {
      loader.dismiss();
      console.log('respuesta: ', data);
      this.presentAlert('Suscripción confirmada');
    }).catch((error) => {
      loader.dismiss();
      console.log('error: ', error);
      this.presentAlert('Error, intentalo de nuevo');
    });
  }

  async deleteNewsletter(email: any) {

    const loader = await this.loadingController.create({
      message: 'Cancelando suscripción...',
    });
    await loader.present();

    this.dataService.deleteNewsletter(email).toPromise().then((data) => {
      loader.dismiss();
      console.log('respuesta: ', data);
      this.presentAlert('Suscripción cancelada');
    }).catch((error) => {
      loader.dismiss();
      console.log('error: ', error);
      this.presentAlert('Error, intentalo de nuevo');
    });
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navController.navigateForward('/');
          }
        }
      ],
    });

    await alert.present();
  }

  async presentToast( message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 1500,
      mode: 'ios',
      position: 'middle',
    });
    toast.present();
  }

}
