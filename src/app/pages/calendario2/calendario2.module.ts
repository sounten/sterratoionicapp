import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Calendario2Page } from './calendario2.page';
import { ComponentsModule } from '../../components/components.module';
import { CalendarModule } from 'ion2-calendar';
import { ServiciosPageModule } from './servicios/servicios.module';
import { ServiciosPage } from './servicios/servicios.page';

const routes: Routes = [
  {
    path: '',
    component: Calendario2Page,
  }
];

@NgModule({
  entryComponents: [
    ServiciosPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    CalendarModule,
    ServiciosPageModule,
  ],
  declarations: [Calendario2Page],
  providers: [ { provide: LOCALE_ID, useValue: 'es-ES' }]
})
export class Calendario2PageModule {}
