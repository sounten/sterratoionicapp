import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage implements OnInit {

  servicios: any[];

  constructor(private modalCtrl: ModalController,
              private storage: Storage) { }

  ngOnInit() {
    this.addServices();
  }

  addServices() {
    this.getStorageData();
  }

  getStorageData() {

    this.storage.get('data').then((val) => {
      this.servicios = JSON.parse(val).services;
    });
  }

  salirConArgumentos( nombreServicio: string,
                      duracion: string) {
    this.modalCtrl.dismiss({
      nombre: nombreServicio ,
      duracion: String(duracion.replace(' min.', '')),
    });
  }

}
