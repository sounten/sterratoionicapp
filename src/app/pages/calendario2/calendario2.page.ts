import { Component, OnInit } from '@angular/core';
import { CalendarComponentOptions } from 'ion2-calendar';
import * as moment from 'moment';
import { ModalController, ToastController, AlertController, NavController, LoadingController } from '@ionic/angular';
import { ServiciosPage } from './servicios/servicios.page';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-calendario2',
  templateUrl: './calendario2.page.html',
  styleUrls: ['./calendario2.page.scss'],
})
export class Calendario2Page implements OnInit {

  fromServiceModal: any;
  timeButtons;
  horasJSONRecv: any;

  nombreServicio = 'Selecciona servicio';
  duracionServicio = 0;
  todayDate = new Date();
  dayId = moment(this.todayDate).format('YYYYMMDD');
  date = this.dayId;
  infoServicios;
  timeJSON;

  type: 'js-date';
  optionsMonth: CalendarComponentOptions = {
    showMonthPicker: false,
    weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekStart: 1
  };

  constructor(public modalCtrl: ModalController,
              private toastController: ToastController,
              public router: Router,
              private alertController: AlertController,
              private navController: NavController,
              private dataService: DataService,
              private loadingController: LoadingController) {
    this.todayDate = new Date();
    this.dayId = moment(this.todayDate).format('YYYYMMDD');
    this.loadDataFromSterrato();
    console.log('constructor dayId:', this.dayId);
  }

  ngOnInit() {
  }

  async presentToast( message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 1500,
      mode: 'ios',
      position: 'middle',
      buttons: [
        {
          text: 'Seleccionar',
          handler: () => {
            this.seleccionarServicio();
          }
        }
      ]
    });
    toast.present();
  }

  async seleccionarServicio() {
    const modal = await this.modalCtrl.create({
      component: ServiciosPage,
      componentProps: {
    }
    });

    await modal.present();

    this.fromServiceModal = await modal.onDidDismiss();
    if (this.fromServiceModal.data) {
      this.nombreServicio = this.fromServiceModal.data.nombre;
      this.duracionServicio = this.fromServiceModal.data.duracion;
      this.infoServicios = this.fromServiceModal.data;
      this.addTimeButtons();
    }
    // console.log('Retorno del modal->', this.fromServiceModal);
  }

  goToForm(hour: string) {

    if (this.fromServiceModal == null) {
      this.presentToast('Selecciona un servicio para poder continuar');
    } else {
      const datosJSON = { DayId: this.dayId,
                          Time: hour,
                          Service: this.nombreServicio,
                          Duration: this.duracionServicio,
                          };
      console.log('datos servicio:', datosJSON);
      const datosJSONString = JSON.stringify(datosJSON);
      this.router.navigate(['formulario', datosJSONString]);
    }
  }

  async onChange( event: Date ) {
    this.dayId = String(moment(event).format('YYYYMMDD'));
    await this.loadDataFromSterrato();
  }

  async loadDataFromSterrato() {
    const loader = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loader.present();

    await this.dataService.getDataSterrato(this.dayId).subscribe((data) => {

      loader.dismiss();
      this.timeJSON = data;
      this.addTimeButtons();

      console.log('timeJSON recibido: ', this.timeJSON);
    },
    (error) => {
      loader.dismiss();
      console.log('error: ', error);
      this.presentAlert('Error, intentalo de nuevo');
    });
  }

  addTimeButtons() {
    this.timeButtons = [];

    if (this.timeJSON !== undefined && this.timeJSON.Horario !== undefined) {
      const minOrderTime = 15;

      this.timeJSON.Horario.forEach(element => {
        const open = Number(element.Open);
        const close = Number(element.Close);

        for (let i = open; i <= close - minOrderTime; i += minOrderTime) {
          if (i % 100 === 60) { i += 40; }
          this.timeButtons.push({Time: i, Enabled: true});
      }
      });
      if (this.timeJSON.Reservas !== undefined) {
        this.timeJSON.Reservas.forEach(element => {
          let horaNum;
          horaNum = element.Hora;
          horaNum = String(horaNum.replace(':', ''));
          if (horaNum.charAt(0) === '0') {
            horaNum = horaNum.substr(1);
          }
          this.disableBookedTimeslots(Number(horaNum), Number(element.Duracion), minOrderTime);
        });

      }
      this.disableImpossibleTimeslots(this.duracionServicio, minOrderTime);
      this.formatButtonLabels();
    }
  }

  disableBookedTimeslots(time: number, duration: number, minOrderTime: number) {
    for (let i = 0; i <= duration - 15; i += minOrderTime) {
      if (time % 100 === 60 ) { time += 40; }
// tslint:disable-next-line: prefer-for-of
      for (let j = 0; j < this.timeButtons.length; j++) {
        if (this.timeButtons[j].Time === time ) {
          this.timeButtons[j].Enabled = false;
        }
      }
      time += minOrderTime;
    }
  }

  disableImpossibleTimeslots(duracion: number, minOrderTime: number) {
    // minOrderTime should probably be a constant somewhere. makes no sense to pass a value that
    // will never change.
    let cabe = true;
    let nextTimeButton;
    let cont;

    for (let i = 0; i < this.timeButtons.length; i++) {
      if (this.timeButtons[i].Enabled === true && duracion > 15 ) {
        cont = 1;
        cabe = true;
        for (let j = minOrderTime; j <= duracion - minOrderTime ; j += minOrderTime) {
          nextTimeButton = i + cont;
          if (nextTimeButton < this.timeButtons.length) {
            // console.log('next timebutton check:' , this.timeButtons[nextTimeButton].Enabled);
            if (this.timeButtons[nextTimeButton].Enabled === false) { cabe = false; }
          } else {
            cabe = false;
          }
          cont++;
        }
      }
      // console.log('salgo del segundo for, hora check:',  this.timeButtons[i].Time);
      if (!cabe) { this.timeButtons[i].Enabled = false; }
    }
  }

  formatButtonLabels() {
    let timeSTR: string;
// tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.timeButtons.length; i++) {
      timeSTR = this.timeButtons[i].Time.toString();
      if (timeSTR.length < 4) {timeSTR = '0' + timeSTR; }
      timeSTR = timeSTR.substring(0, 2) + ':' + timeSTR.substring(2);
      this.timeButtons[i].Time = timeSTR;
    }
  }


  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navController.navigateForward('/');
          }
        }
      ],
    });

    await alert.present();
  }

}
