import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import {  AlertController, LoadingController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.page.html',
  styleUrls: ['./formulario.page.scss'],
})
export class FormularioPage implements OnInit {

  @Input()

  emailPattern = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);
  phonePattern = new RegExp(/^([+]?[0-9]{9}[0-9]*3?$)/);

  isNombre = false;
  isEmail = false;
  isPhone = false;

  diaReserva;

  userData = {
    name: '',
    email: '',
    phone: '',
    comment: ''
  };

  dayId;
  datosReserva;

  constructor(public activatedRoute: ActivatedRoute,
              private alertController: AlertController,
              private loadingController: LoadingController,
              private navController: NavController,
              public httpClient: HttpClient,
              private dataService: DataService) { }

  ngOnInit() {
    const dataRecv = this.activatedRoute.snapshot.paramMap.get('data');
    const dataReserva = JSON.parse(dataRecv);
    this.datosReserva = dataReserva;
    console.log(dataReserva.DayId);
    this.dayId = dataReserva.DayId;
    this.diaReserva = ((dataReserva.DayId).toString());
    this.formatDateProperly();
    console.log('info JSON:', this.datosReserva);
    console.log('info dia:', this.diaReserva);
  }

  formatDateProperly() {
    const aux = [];
    aux[1] = this.diaReserva.substring(0, 4);
    aux[2] = this.diaReserva.substring(4, 6);
    aux[3] = this.diaReserva.substring(6);
    this.diaReserva = aux[3] + '-' + aux[2] + '-' + aux[1];
  }

  isValidName() {
    this.isNombre = this.userData.name === '';
  }

  isValidEmail() {
    this.isEmail = !this.emailPattern.test(this.userData.email);
  }

  isValidPhone() {
    this.isPhone = !this.phonePattern.test(this.userData.phone);
  }

  onSubmitForm() {
    this.isValidName();
    this.isValidEmail();
    this.isValidPhone();
    if (!this.isNombre && !this.isEmail && !this.isPhone) {
      //delete this.datosReserva.DayId;
      const fullData = {  name: this.userData.name,
                          email: this.userData.email,
                          phone: this.userData.phone,
                          comment: this.userData.comment,
                          dayId: this.datosReserva.DayId,
                          Duration: this.datosReserva.Duration,
                          Service: this.datosReserva.Service,
                          Time: this.datosReserva.Time
                        };
      console.log('full datos:', fullData);
      this.postBooking(fullData);
    }
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navController.navigateForward('/');
          }
        }
      ],
    });

    await alert.present();
  }

  async postBooking(fullData: any) {

    const loader = await this.loadingController.create({
      message: 'Reservando...',
    });
    await loader.present();

    this.dataService.postDataReserva(fullData, this.dayId).toPromise().then((data) => {
      loader.dismiss();
      console.log('respuesta: ', data);
      this.presentAlert('Reserva realizada');
    }).catch((error) => {
      loader.dismiss();
      console.log('error: ', error);
      this.presentAlert('Error, intentalo de nuevo');
    });
  }
}
