import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

const headers = new HttpHeaders( {
  responseType: 'text',
});

@Injectable({
  providedIn: 'root'
})
export class DataService {

  pathServicios = 'https://c4ru9606c5.execute-api.eu-west-3.amazonaws.com/prod/api/services';
  pathReservas = 'https://c4ru9606c5.execute-api.eu-west-3.amazonaws.com/prod/api/monitor/environmentvariables';
  pathEnvioSolicitudReserva = 'https://c4ru9606c5.execute-api.eu-west-3.amazonaws.com/prod/api/reservas/';
  pathHorarioYReservas = 'https://c4ru9606c5.execute-api.eu-west-3.amazonaws.com/prod/api/reservas/' ;
  pathNewsletter = 'https://c4ru9606c5.execute-api.eu-west-3.amazonaws.com/prod/api/newsletter/subscriptions/';

  constructor(private httpClient: HttpClient,
              private storage: Storage) { }

  getDataSterrato(dayId: string) {
    return this.httpClient.get(this.pathHorarioYReservas + dayId);
  }

  postDataReserva(datosReserva: any, dayId) {
    return this.httpClient.post(this.pathEnvioSolicitudReserva + dayId, datosReserva);
  }

  getServices() {
    this.httpClient.get(this.pathServicios).subscribe((data) => {
      this.storage.set('data', JSON.stringify(data));
    });
  }

  postNewsletter(email: string) {
    return this.httpClient.post(this.pathNewsletter + email + '/', null);
  }

  deleteNewsletter(email: string) {
    return this.httpClient.delete(this.pathNewsletter + email + '/');
  }

}
