import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy, AfterViewInit {
  backButtonSubscription;
  horasJSONRecv: any;
  jsones: any = [];

  constructor(public storage: Storage,
              private dataService: DataService,
              private router: Router,
              private loadingController: LoadingController,
              private platform: Platform) {
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      if (window.location.pathname == "/home") {
        navigator['app'].exitApp();
      }
    });
  }
  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  goToNewsletter() {
    this.router.navigate(['newsletter']);
  }

  async goToCalendar() {

    const loader = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loader.present();
    this.dataService.getServices();
    loader.dismiss();
    this.router.navigate(['calendario2']);
  }

}

